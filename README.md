# blended-learning-front-end

This project is the application that connect to the API to allow the users to login and create the content.

Before you can start this application you will need to make sure you have set up the API to run from port 3001

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
