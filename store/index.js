// Create a Store
export const state = () => ({
  posts: { }
})

// Getters
export const getters = {
  posts (state) {
    return state.posts
  },

  authenticated (state) {
    return state.loggedIn
  },

  user (state) {
    return state.user
  }
}

// Mutations
export const mutations = {
  SET_POST (state, posts) {
    state.posts = posts
  }
}

// Actions
export const actions = {
  setPosts ({ commit }, posts) {
    commit('SET_POSTS', posts)
  }
}
