// States
export const state = () => ({
  openMenu: false,
  openUserDetails: false
})

// mutations
export const mutations = {

  TOGGLE_MENU (state) {
    state.openMenu = !state.openMenu
    state.openUserDetails = false
  },

  TOGGLE_USER_DETAILS (state) {
    state.openUserDetails = !state.openUserDetails
    state.openMenu = false
  },

  CLOSE_MENU_AND_DETAILS (state) {
    state.openMenu = false
    state.openUserDetails = false
  }
}

// actions
export const actions = {

  toggleMenu ({ commit }) {
    commit('TOGGLE_MENU')
  },

  toggleUserDetails ({ commit }) {
    commit('TOGGLE_USER_DETAILS')
  },

  closeMenuAndDetails ({ commit }) {
    commit('CLOSE_MENU_AND_DETAILS')
  }
}

// Getters
export const getters = {
  toggleMenu: state => state.openMenu,
  toggleUserDetails: state => state.openUserDetails
}
