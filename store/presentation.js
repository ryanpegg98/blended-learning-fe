// States
export const state = () => ({
  closePath: '',
  lessonId: '',
  nextPage: '',
  prevPage: ''
})

// mutations
export const mutations = {
  SET_CLOSE_PATH (state, path) {
    state.closePath = path
  },
  SETUP_PRESENTATION (state, { path, lesson }) {
    state.closePath = path
    state.lessonId = lesson
    state.nextPage = `/present/${lesson}/contents`
    state.prevPage = ''
  },
  SET_PAGES (state, { nextPath, prevPath }) {
    state.nextPage = nextPath
    state.prevPage = prevPath
  }
}

// actions
export const actions = {
  setClosePath ({ commit }, path) {
    commit('SET_CLOSE_PATH', path)
  },
  setupPresentation ({ commit }, params) {
    commit('SETUP_PRESENTATION', params)
  },
  setPages ({ commit }, params) {
    commit('SET_PAGES', params)
  }
}

// Getters
export const getters = {
  currentClosePath: state => state.closePath,
  lesson: state => state.lessonId,
  currentNextPath: state => state.nextPage,
  currentPrevPath: state => state.prevPage
}
