let development = process.env.NODE_ENV !== 'production'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Blended Learning',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A platform desgined to deliver the content you create for in classroom and remote sessions.' },
      { name: 'theme-color', content: '#364363' },
      { name: 'msapplication-TileColor', contnet: '#364363'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://kit.fontawesome.com/c60aaa85e9.js', crossorigin: 'anonymous' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~assets/scss/main.scss',
    'simplemde/dist/simplemde.min.css',
  ],

  styleResources: {
    scss: [
      '~assets/scss/main.scss'
    ]
  },
  /*
  ** Add the router config to add default middleware values
  */
 router: {
   middleware: ["clearValidationErrors"]
 },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    "./plugins/axios.js",
    "./plugins/mixins/validation.js",
    { src: './plugins/nuxt-simplemde-plugin.js', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/style-resources',
    '@nuxtjs/markdownit'
  ],
  /*
  ** Configuration for the markdownit module
  */
 markdownit: {
   injected: true
 },
  /*
  * AXIOS module configuration
  * Set the base URL to be based on the environment
  */
  axios: {
    baseURL: development ? 'http://localhost:3001/api/v1' : 'http://api.nitrosystems.co.uk/api/v1'
  },
  /*
  * Auth module configuration
  */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/login', method: 'post', propertyName: 'token' },
          logout: { url: '/logout', method: 'delete' },
          user: { url: '/user', method: 'get', propertyName: 'user' }
        },
        // tokenRequired: true,
        // tokenType: 'bearer'
        // autoFetchUser: true
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
