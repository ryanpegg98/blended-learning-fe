// This middleware will clear the validations when navigating pages

export default function ({ store }) {
  store.dispatch('validation/clearErrors')
  /*
  * Close the menu everytime the user is moved to a different location. Otherwise
  * the users might be confused as the screen might not change
  */
  store.dispatch('menu/closeMenuAndDetails')
}
