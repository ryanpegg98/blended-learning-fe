// Create an email address that is always different
const timestamp = Date.now()
const email_address = "testuser-" + timestamp + "@example.com"

// This spec will test that the user will be able to sign up to the platform
describe("A user should be able to sign up", () => {
  // When all the data is correct allow the user should be logged in
  it("The user should be logged in when all the data is correct", () => {
    // Go to the project
    cy.visit("http://localhost:3000")
    // Try and find a sign up button
    cy.get('A')
      .contains("Sign Up")
      .click()

    // The URL should now be sign up
    cy.url().should('include', '/sign_up')

    // Enter the name for the users
    cy.get('[name=first_name]')
      .type("Test")
      .should('have.value', "Test")
    cy.get('[name=last_name]')
      .type("Signup")
      .should('have.value', "Signup")
    cy.get('[name=email]')
      .type(email_address)
      .should('have.value', email_address)
    cy.get('[name=password]')
      .type("password")
      .should('have.value', "password")
    cy.get('[name=password_confirmation]')
      .type("password")
      .should('have.value', "password")

    // Try and submit the form
    cy.get("BUTTON[type=submit]")
      .contains("Sign up")
      .click()

    // The page url should now be the dashboard
    cy.url().should('include', '/dashboard')
  })

  it("A user should be able to enter the sign up code", () => {
    cy.contains("Join an Organisation").click()

    cy.get("[name=organisation_code]")
      .type("test_code")
      .should("have.value", "test_code")

    cy.get("BUTTON")
      .contains("Join Organisation")
      .click()

    cy.contains("Edge Hill University")

    cy.contains("Yes").click()

    cy.contains("Edge Hill University")

    cy.get('.open_menu')
      .click()

    cy.contains("Organisations").click()

    cy.contains("Edge Hill University")
  })

  it("A user should be able to sign up to a course that is an open course", () => {
    cy.contains("Edge Hill University")
      .parent("TR")
      .get("TD:last-child")
      .contains('View')
      .click()

    cy.contains("Open Course")
      .parent("DIV")
      .parent("LI")
      .children("A")
      .contains("Join")
      .click()

    cy.contains("Open Course")

    cy.contains("Yes")
      .click()

    cy.contains("Open Course")

    cy.get("HEADER A")
      .click()

    cy.contains("Open Course")
  })
})
