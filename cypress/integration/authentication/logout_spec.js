// This spec will make sure that the user can logout of the application
describe('A user should be able to logout', () => {
  // This should allow the user to login and and see the logout button
  it('A valid user can see the logout button and click it', () =>{
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan@cleversteam.com')
      .should('have.value', 'ryan@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')
      .should('have.value', 'password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

    // A user should be redirected to the dashboard
    cy.url().should('include', '/dashboard')
  })
} )