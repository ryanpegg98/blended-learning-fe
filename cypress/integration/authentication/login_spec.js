// This test will check that the user can login to the application
describe('Can the user log into the platform?', () => {
  // Check that there is a login button on the home page
  it('A user should be able to get to the login page.', () => {
    // Go to the home page of the app
    cy.visit('http://localhost:3000')
    // Try and find a button that sayd login and click it
    cy.contains("Login").click()
    // The user should be able to see the login page
    cy.url().should('include', '/login')
  })

  // Test that a valid user can log in
  it('A valid user should be able to login and be redirect to their dashboard', () => {
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan@cleversteam.com')
      .should('have.value', 'ryan@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')
      .should('have.value', 'password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

    // A user should be redirected to the dashboard
    cy.url().should('include', '/dashboard')
  })

  // Make sure a user cannot login again
  it('A user should not be able to access the login page', () => {
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan@cleversteam.com')
      .should('have.value', 'ryan@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')
      .should('have.value', 'password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

   // A user should be redirected to the dashboard
   cy.url().should('include', '/dashboard')

   // Try and get back to the login page
   cy.visit('http://localhost:3000/login')

   // A user should be redirected to the dashboard
   cy.url().should('include', '/dashboard')
  })
})