// Create an email address that is always different
const timestamp = Date.now()
const email_address = "testuser-" + timestamp + "@example.com"

// This spec will test that the user will be able to sign up to the platform
describe("A user should be able to sign up", () => {
  // Check that the user does can see all of the validation
  it("The user should be able to see all the blank errors", () => {
    // Go to the project
    cy.visit("http://localhost:3000")
    // Try and find a sign up button
    cy.get('A')
      .contains("Sign Up")
      .click()

    // The URL should now be sign up
    cy.url().should('include', '/sign_up')

    // Try and submit the form
    cy.get("BUTTON[type=submit]")
      .contains("Sign up")
      .click()

    // The page url should not have changed
    cy.url().should('include', '/sign_up')

    // The pages should now show a number of errors
    cy.contains("First Name can't be blank")
    cy.contains("Last Name can't be blank")
    cy.contains("Email Address can't be blank")
    cy.contains("Password can't be blank")
  })

  // Check that the user cannot enter the same email and password that doesn't match
  it("The user should see the email taken and password errors", () => {
    // Go to the project
    cy.visit("http://localhost:3000")
    // Try and find a sign up button
    cy.get('A')
      .contains("Sign Up")
      .click()

    // The URL should now be sign up
    cy.url().should('include', '/sign_up')

    // Enter the name for the users
    cy.get('[name=first_name]')
      .type("Test")
      .should('have.value', "Test")
    cy.get('[name=last_name]')
      .type("Signup")
      .should('have.value', "Signup")

    // The email should be one that is already being used
    cy.get('[name=email]')
      .type("ryan@cleversteam.com")
      .should('have.value', "ryan@cleversteam.com")

    // The password fields should not match
    cy.get('[name=password]')
      .type("password")
      .should('have.value', "password")
    cy.get('[name=password_confirmation]')
      .type("password123")
      .should('have.value', "password123")

    // Try and submit the form
    cy.get("BUTTON[type=submit]")
      .contains("Sign up")
      .click()

    // The page url should not have changed
    cy.url().should('include', '/sign_up')

    // The following messages should be visible
    cy.contains("Email Address has already been taken")
    cy.contains("Password Confirmation doesn't match Password")
  })

  // When all the data is correct allow the user should be logged in
  it("The user should be logged in when all the data is correct", () => {
    // Go to the project
    cy.visit("http://localhost:3000")
    // Try and find a sign up button
    cy.get('A')
      .contains("Sign Up")
      .click()

    // The URL should now be sign up
    cy.url().should('include', '/sign_up')

    // Enter the name for the users
    cy.get('[name=first_name]')
      .type("Test")
      .should('have.value', "Test")
    cy.get('[name=last_name]')
      .type("Signup")
      .should('have.value', "Signup")
    cy.get('[name=email]')
      .type(email_address)
      .should('have.value', email_address)
    cy.get('[name=password]')
      .type("password")
      .should('have.value', "password")
    cy.get('[name=password_confirmation]')
      .type("password")
      .should('have.value', "password")

    // Try and submit the form
    cy.get("BUTTON[type=submit]")
      .contains("Sign up")
      .click()

    // The page url should now be the dashboard
    cy.url().should('include', '/dashboard')
  })
})
