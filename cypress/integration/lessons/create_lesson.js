const organisation_name = "Edge Hill University"
const timestamp = Date.now()
const course_name = "Course " + timestamp

describe("Can a user create a course", () => {
  it("Check a user can see the new course button", () => {
    // Set the viewport to be that of a macbook
    cy.viewport('samsung-s10')
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan+test@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

    // A user should be redirected to the dashboard
    cy.url().should('include', '/dashboard')

    // A user should be able to see the organistions link
    cy.contains("My Organisations").click()

    cy.contains(organisation_name)
      .parent("TR")
      .get("TD:last-child")
      .contains('View')
      .click()

    // Check that they can see a new course button
    cy.contains("New Course").click()
  })

  it("A user should be able to add the course name", () => {
    cy.get('[name=course_name]')
      .type(course_name)
      .should('have.value', course_name)

    cy.contains("Create Course").click()

    cy.contains("Sign up mode can't be blank")

    cy.get('[name=sign_up_mode]')
      .select('hidden')
      .should('have.value', 'hidden')

    cy.contains("Create Course").click()

    cy.contains(course_name)
  })
})
