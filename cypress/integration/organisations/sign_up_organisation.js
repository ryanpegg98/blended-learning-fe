const timestamp = Date.now()
const new_organisation_name = "Organisation " + timestamp
const user_email = "test-" + timestamp + "@example.com"

// Test that a user can sign up and create an organisation
describe("Can a user sign up and create an organisation", () => {

  it("Can the user sign up as an organisation admin", () => {
    // Set the viewport to be that of an Iphone
    cy.viewport('iphone-x')

    cy.visit("http://localhost:3000/sign_up")

    // Try to get to the organiation sign up page
    cy.contains("Sign up as an organisation").click()

    // Check the URL has changed
    cy.url().should('include', '/organisation-sign-up')

    // Enter the data
    cy.get('[name=organisation_name]')
      .type(new_organisation_name)
      .should('have.value', new_organisation_name)
    cy.get('[name=first_name]')
      .type("Test")
      .should('have.value', "Test")
    cy.get('[name=last_name]')
      .type("Signup")
      .should('have.value', "Signup")
    cy.get('[name=email]')
      .type(user_email)
      .should('have.value', user_email)
    cy.get('[name=password]')
      .type("password")
      .should('have.value', "password")
    cy.get('[name=password_confirmation]')
      .type("password")

    // Attempt to submit the form
    cy.get("BUTTON[type=submit]")
      .click()

    // Go to My organisations
    cy.contains("My Organisations").click()

    // Make sure the list contains the organisation name
    cy.contains(new_organisation_name)

  })
})
// Test that a learner and teacher cannot sign in and create an organisation
