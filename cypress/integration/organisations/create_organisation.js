// Create a unique organisation name
const timestamp = Date.now()
const organisation_name = "Organisation " + timestamp

// Test that a user can login and create a user
describe("Can a user create an organisation", () => {
  it("Can the user create an organisation on a desktop", () => {
    // Set the viewport to be that of a macbook
    cy.viewport('macbook-13')
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

    // A user should be redirected to the dashboard
    cy.url().should('include', '/dashboard')

    // A user should be able to see the organistions link
    cy.get("NAV UL LI A")
      .contains("SPAN", "Organisations")
      .click()

    // The user should now be on the organisations page
    cy.url().should('include', '/organisations')

    // The page should have a create organisation button
    cy.get("A.btn").contains("Create Organisation").click()

    // The page should now be the create page
    cy.url().should('include', '/organisations/create')

    // A user should be able to enter the organisation name
    cy.get('[name=organisation_name]')
      .type(organisation_name)
      .should('have.value', organisation_name)

    // Set the sign_up_method
    cy.get('[name=sign_up_method]')
      .select('no_sign_up')
      .should('have.value', 'no_sign_up')

    // A user should be able to see and click the button to create the organisation
    cy.get("BUTTON[type=submit]")
      .contains("Create Organisation")
      .click()

    // A user should be redirected to the index page
    cy.url().should('include', '/organisations')

    // They should be able to see the organisation they created
    cy.get(" TR TD:first-child")
      .contains(organisation_name)
  })
})
