const organisation_name = "Edge Hill University"

describe("Can a user edit an organisation", () => {
  it("Can the user get to the edit page", () => {
    // Set the viewport to be that of a macbook
    cy.viewport('ipad-2')
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan+test@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

    // A user should be redirected to the dashboard
    cy.url().should('include', '/dashboard')

    // A user should be able to see the organistions link
    cy.contains("My Organisations").click()

    cy.contains(organisation_name)
      .parent("TR")
      .get("TD:last-child")
      .contains('View')
      .click()

    // Check that they can see a edit button
    cy.contains("Edit").click()

    cy.get('[name=sign_up_code]')
      .clear()
      .type("test_code")
      .should('have.value', 'test_code')

    cy.contains("Update Organisation")
      .click()

    cy.url().should('not.contain', 'edit')
  })
})
