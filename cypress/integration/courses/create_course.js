const organisation_name = "Edge Hill University"
const timestamp = Date.now()
const course_name = "Course " + timestamp

describe("Can a user create a course", () => {
  it("Check a user can see the new course button", () => {
    // Set the viewport to be that of a macbook
    cy.viewport('samsung-s10')
    // Go to the login page
    cy.visit('http://localhost:3000/login')
    // They should be able to see the labels fo email and password
    cy.contains("Email")
    cy.contains("Password")
    // A user should be able to enter their email
    cy.get('[type=email]')
      .type('ryan+test@cleversteam.com')

    // A user should be able to enter their password
    cy.get('[type=password]')
      .type('password')

    // A user should be able to click the login button
    cy.get('BUTTON')
      .contains('Login')
      .click()

    // A user should be redirected to the dashboard
    cy.url().should('include', '/dashboard')

    // A user should be able to see the organistions link
    cy.contains("My Organisations").click()

    cy.contains(organisation_name)
      .parent("TR")
      .get("TD:last-child")
      .contains('View')
      .click()

    // Check that they can see a new course button
    cy.contains("New Course").click()
  })

  it("A user should be able to add the course name", () => {
    cy.get('[name=course_name]')
      .type(course_name)
      .should('have.value', course_name)

    cy.contains("Create Course").click()

    cy.contains("Sign up mode can't be blank")

    cy.get('[name=sign_up_mode]')
      .select('hidden')
      .should('have.value', 'hidden')

    cy.contains("Create Course").click()

    cy.contains(course_name)
  })

  const lesson_one = "Lesson One " + timestamp
  const lesson_two = "Lesson Two " + timestamp
  const lesson_three = "Lesson Three " + timestamp

  it("A user should be able to add three lessons to the course", () => {

    cy.contains("New Lesson").click()

    cy.get('[name=lesson_name]')
      .type(lesson_one)
      .should('have.value', lesson_one)

    cy.contains("Create Lesson").click()

    cy.contains(lesson_one)

    cy.contains("New Lesson").click()

    cy.get('[name=lesson_name]')
      .type(lesson_two)
      .should('have.value', lesson_two)

    cy.contains("Create Lesson").click()

    cy.contains(lesson_two)

    cy.contains("New Lesson").click()

    cy.get('[name=lesson_name]')
      .type(lesson_three)
      .should('have.value', lesson_three)

    cy.contains("Create Lesson").click()

    cy.contains(lesson_three)
  })

  it("A user should be able to add sections to the lessons", () => {
    cy.contains(lesson_one)
      .click()

    cy.get("[name=section_title]")
      .type("Section One")
      .should('have.value', "Section One")

    cy.contains("Create Section").click()

    cy.contains("Section One")

    cy.get("[name=section_title]")
    .should('not.have.value', "Section One")

    cy.contains("Section One")

    cy.get("[name=section_title]")
      .type("Section Two")
      .should('have.value', "Section Two")

    cy.contains("Create Section").click()

    cy.contains("Section Two")

    cy.get("[name=section_title]")
    .should('not.have.value', "Section Two")

    cy.contains("Section Two")
  })

  it("A user should not be able to add sections without a title", () => {
    cy.contains("Create Section").click()

    cy.contains("Title can't be blank")
  })

  it("A user should be able to delete a section", () => {
    cy.get("[name=section_title]")
      .type("Section Delete")
      .should('have.value', "Section Delete")

    cy.contains("Create Section").click()

    cy.contains("Section Delete")

    cy.get("[name=section_title]")
    .should('not.have.value', "Section Delete")

    cy.contains("Section Delete")
      .parent("LI")
      .children(".delete")
      .click()

    cy.contains("Back").click()

    cy.get('.lessons_list').contains(lesson_one).click()

    cy.contains("Section Delete").should('not.exist')
    cy.contains(lesson_one)
  })
})
